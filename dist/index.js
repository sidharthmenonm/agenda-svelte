(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('luxon'), require('svelte-markdown'), require('simplur')) :
    typeof define === 'function' && define.amd ? define(['luxon', 'svelte-markdown', 'simplur'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.Agenda = factory(global.luxon, global.SvelteMarkdown, global.simplur));
})(this, (function (luxon, SvelteMarkdown, simplur) { 'use strict';

    function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

    var SvelteMarkdown__default = /*#__PURE__*/_interopDefaultLegacy(SvelteMarkdown);
    var simplur__default = /*#__PURE__*/_interopDefaultLegacy(simplur);

    function noop() { }
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    let src_url_equal_anchor;
    function src_url_equal(element_src, url) {
        if (!src_url_equal_anchor) {
            src_url_equal_anchor = document.createElement('a');
        }
        src_url_equal_anchor.href = url;
        return element_src === src_url_equal_anchor.href;
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }
    function create_slot(definition, ctx, $$scope, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, $$scope, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, $$scope, fn) {
        return definition[1] && fn
            ? assign($$scope.ctx.slice(), definition[1](fn(ctx)))
            : $$scope.ctx;
    }
    function get_slot_changes(definition, $$scope, dirty, fn) {
        if (definition[2] && fn) {
            const lets = definition[2](fn(dirty));
            if ($$scope.dirty === undefined) {
                return lets;
            }
            if (typeof lets === 'object') {
                const merged = [];
                const len = Math.max($$scope.dirty.length, lets.length);
                for (let i = 0; i < len; i += 1) {
                    merged[i] = $$scope.dirty[i] | lets[i];
                }
                return merged;
            }
            return $$scope.dirty | lets;
        }
        return $$scope.dirty;
    }
    function update_slot_base(slot, slot_definition, ctx, $$scope, slot_changes, get_slot_context_fn) {
        if (slot_changes) {
            const slot_context = get_slot_context(slot_definition, ctx, $$scope, get_slot_context_fn);
            slot.p(slot_context, slot_changes);
        }
    }
    function get_all_dirty_from_scope($$scope) {
        if ($$scope.ctx.length > 32) {
            const dirty = [];
            const length = $$scope.ctx.length / 32;
            for (let i = 0; i < length; i++) {
                dirty[i] = -1;
            }
            return dirty;
        }
        return -1;
    }
    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function svg_element(name) {
        return document.createElementNS('http://www.w3.org/2000/svg', name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_data(text, data) {
        data = '' + data;
        if (text.wholeText !== data)
            text.data = data;
    }
    function custom_event(type, detail, bubbles = false) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, bubbles, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error('Function called outside component initialization');
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail);
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
            }
        };
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    // flush() calls callbacks in this order:
    // 1. All beforeUpdate callbacks, in order: parents before children
    // 2. All bind:this callbacks, in reverse order: children before parents.
    // 3. All afterUpdate callbacks, in order: parents before children. EXCEPT
    //    for afterUpdates called during the initial onMount, which are called in
    //    reverse order: children before parents.
    // Since callbacks might update component values, which could trigger another
    // call to flush(), the following steps guard against this:
    // 1. During beforeUpdate, any updated components will be added to the
    //    dirty_components array and will cause a reentrant call to flush(). Because
    //    the flush index is kept outside the function, the reentrant call will pick
    //    up where the earlier call left off and go through all dirty components. The
    //    current_component value is saved and restored so that the reentrant call will
    //    not interfere with the "parent" flush() call.
    // 2. bind:this callbacks cannot trigger new flush() calls.
    // 3. During afterUpdate, any updated components will NOT have their afterUpdate
    //    callback called a second time; the seen_callbacks set, outside the flush()
    //    function, guarantees this behavior.
    const seen_callbacks = new Set();
    let flushidx = 0; // Do *not* move this inside the flush() function
    function flush() {
        const saved_component = current_component;
        do {
            // first, call beforeUpdate functions
            // and update components
            while (flushidx < dirty_components.length) {
                const component = dirty_components[flushidx];
                flushidx++;
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            flushidx = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        seen_callbacks.clear();
        set_current_component(saved_component);
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor, customElement) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        if (!customElement) {
            // onMount happens before the initial afterUpdate
            add_render_callback(() => {
                const new_on_destroy = on_mount.map(run).filter(is_function);
                if (on_destroy) {
                    on_destroy.push(...new_on_destroy);
                }
                else {
                    // Edge case - component was destroyed immediately,
                    // most likely as a result of a binding initialising
                    run_all(new_on_destroy);
                }
                component.$$.on_mount = [];
            });
        }
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, append_styles, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            on_disconnect: [],
            before_update: [],
            after_update: [],
            context: new Map(options.context || (parent_component ? parent_component.$$.context : [])),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false,
            root: options.target || parent_component.$$.root
        };
        append_styles && append_styles($$.root);
        let ready = false;
        $$.ctx = instance
            ? instance(component, options.props || {}, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor, options.customElement);
            flush();
        }
        set_current_component(parent_component);
    }
    /**
     * Base class for Svelte components. Used when dev=false.
     */
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    /* src/Detailed/Details.svelte generated by Svelte v3.47.0 */

    function get_each_context$5(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[3] = list[i][0];
    	child_ctx[4] = list[i][1];
    	return child_ctx;
    }

    function get_each_context_1$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[7] = list[i].name;
    	child_ctx[8] = list[i].designation;
    	child_ctx[9] = list[i].organisation;
    	child_ctx[10] = list[i].photo;
    	child_ctx[11] = list[i].linkedin;
    	return child_ctx;
    }

    // (23:0) {#if agenda.description || agenda.link}
    function create_if_block_4$1(ctx) {
    	let div;
    	let t;
    	let current;
    	let if_block0 = /*agenda*/ ctx[0].description && create_if_block_6$1(ctx);
    	let if_block1 = /*agenda*/ ctx[0].link && create_if_block_5$1(ctx);

    	return {
    		c() {
    			div = element("div");
    			if (if_block0) if_block0.c();
    			t = space();
    			if (if_block1) if_block1.c();
    			attr(div, "class", "event__description");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			if (if_block0) if_block0.m(div, null);
    			append(div, t);
    			if (if_block1) if_block1.m(div, null);
    			current = true;
    		},
    		p(ctx, dirty) {
    			if (/*agenda*/ ctx[0].description) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);

    					if (dirty & /*agenda*/ 1) {
    						transition_in(if_block0, 1);
    					}
    				} else {
    					if_block0 = create_if_block_6$1(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(div, t);
    				}
    			} else if (if_block0) {
    				group_outros();

    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});

    				check_outros();
    			}

    			if (/*agenda*/ ctx[0].link) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_5$1(ctx);
    					if_block1.c();
    					if_block1.m(div, null);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(if_block0);
    			current = true;
    		},
    		o(local) {
    			transition_out(if_block0);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			if (if_block0) if_block0.d();
    			if (if_block1) if_block1.d();
    		}
    	};
    }

    // (25:4) {#if agenda.description}
    function create_if_block_6$1(ctx) {
    	let sveltemarkdown;
    	let current;

    	sveltemarkdown = new SvelteMarkdown__default["default"]({
    			props: { source: /*agenda*/ ctx[0].description }
    		});

    	return {
    		c() {
    			create_component(sveltemarkdown.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(sveltemarkdown, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const sveltemarkdown_changes = {};
    			if (dirty & /*agenda*/ 1) sveltemarkdown_changes.source = /*agenda*/ ctx[0].description;
    			sveltemarkdown.$set(sveltemarkdown_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(sveltemarkdown.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(sveltemarkdown.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(sveltemarkdown, detaching);
    		}
    	};
    }

    // (29:4) {#if agenda.link}
    function create_if_block_5$1(ctx) {
    	let div;
    	let a;
    	let t_value = /*agenda*/ ctx[0].link_text + "";
    	let t;
    	let a_href_value;

    	return {
    		c() {
    			div = element("div");
    			a = element("a");
    			t = text(t_value);
    			attr(a, "href", a_href_value = /*agenda*/ ctx[0].link);
    			attr(a, "target", "_blank");
    			attr(div, "class", "event__link");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, a);
    			append(a, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*agenda*/ 1 && t_value !== (t_value = /*agenda*/ ctx[0].link_text + "")) set_data(t, t_value);

    			if (dirty & /*agenda*/ 1 && a_href_value !== (a_href_value = /*agenda*/ ctx[0].link)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (40:0) {#if Object.entries(agenda.speakers).length}
    function create_if_block_2$1(ctx) {
    	let div;
    	let each_value = Object.entries(/*agenda*/ ctx[0].speakers);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$5(get_each_context$5(ctx, each_value, i));
    	}

    	return {
    		c() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "event__speakers");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*Object, agenda, simplur*/ 1) {
    				each_value = Object.entries(/*agenda*/ ctx[0].speakers);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$5(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$5(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (60:16) {#if linkedin}
    function create_if_block_3$1(ctx) {
    	let div;
    	let a;
    	let svg;
    	let path0;
    	let rect;
    	let line0;
    	let line1;
    	let line2;
    	let path1;
    	let a_href_value;

    	return {
    		c() {
    			div = element("div");
    			a = element("a");
    			svg = svg_element("svg");
    			path0 = svg_element("path");
    			rect = svg_element("rect");
    			line0 = svg_element("line");
    			line1 = svg_element("line");
    			line2 = svg_element("line");
    			path1 = svg_element("path");
    			attr(path0, "stroke", "none");
    			attr(path0, "d", "M0 0h24v24H0z");
    			attr(path0, "fill", "none");
    			attr(rect, "x", "4");
    			attr(rect, "y", "4");
    			attr(rect, "width", "16");
    			attr(rect, "height", "16");
    			attr(rect, "rx", "2");
    			attr(line0, "x1", "8");
    			attr(line0, "y1", "11");
    			attr(line0, "x2", "8");
    			attr(line0, "y2", "16");
    			attr(line1, "x1", "8");
    			attr(line1, "y1", "8");
    			attr(line1, "x2", "8");
    			attr(line1, "y2", "8.01");
    			attr(line2, "x1", "12");
    			attr(line2, "y1", "16");
    			attr(line2, "x2", "12");
    			attr(line2, "y2", "11");
    			attr(path1, "d", "M16 16v-3a2 2 0 0 0 -4 0");
    			attr(svg, "xmlns", "http://www.w3.org/2000/svg");
    			attr(svg, "class", "icon icon-tabler icon-tabler-brand-linkedin");
    			attr(svg, "width", "24");
    			attr(svg, "height", "24");
    			attr(svg, "viewBox", "0 0 24 24");
    			attr(svg, "stroke-width", "2");
    			attr(svg, "stroke", "currentColor");
    			attr(svg, "fill", "none");
    			attr(svg, "stroke-linecap", "round");
    			attr(svg, "stroke-linejoin", "round");
    			attr(a, "href", a_href_value = /*linkedin*/ ctx[11]);
    			attr(a, "target", "_blank");
    			attr(div, "class", "linkedin");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, a);
    			append(a, svg);
    			append(svg, path0);
    			append(svg, rect);
    			append(svg, line0);
    			append(svg, line1);
    			append(svg, line2);
    			append(svg, path1);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*agenda*/ 1 && a_href_value !== (a_href_value = /*linkedin*/ ctx[11])) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (51:10) {#each speaker_list as {name, designation, organisation, photo, linkedin}}
    function create_each_block_1$2(ctx) {
    	let div5;
    	let div0;
    	let img;
    	let img_src_value;
    	let t0;
    	let div4;
    	let div1;
    	let t1_value = /*name*/ ctx[7] + "";
    	let t1;
    	let t2;
    	let div2;
    	let t3_value = /*designation*/ ctx[8] + "";
    	let t3;
    	let t4;
    	let div3;
    	let t5_value = /*organisation*/ ctx[9] + "";
    	let t5;
    	let t6;
    	let t7;
    	let if_block = /*linkedin*/ ctx[11] && create_if_block_3$1(ctx);

    	return {
    		c() {
    			div5 = element("div");
    			div0 = element("div");
    			img = element("img");
    			t0 = space();
    			div4 = element("div");
    			div1 = element("div");
    			t1 = text(t1_value);
    			t2 = space();
    			div2 = element("div");
    			t3 = text(t3_value);
    			t4 = space();
    			div3 = element("div");
    			t5 = text(t5_value);
    			t6 = space();
    			if (if_block) if_block.c();
    			t7 = space();
    			if (!src_url_equal(img.src, img_src_value = /*photo*/ ctx[10])) attr(img, "src", img_src_value);
    			attr(img, "alt", "name");
    			attr(div0, "class", "speakers__photo speakers__photo__circle");
    			attr(div1, "class", "speakers__name");
    			attr(div2, "class", "speakers__designation");
    			attr(div3, "class", "speakers__organisation");
    			attr(div4, "class", "speakers__description");
    			attr(div5, "class", "speakers__item");
    		},
    		m(target, anchor) {
    			insert(target, div5, anchor);
    			append(div5, div0);
    			append(div0, img);
    			append(div5, t0);
    			append(div5, div4);
    			append(div4, div1);
    			append(div1, t1);
    			append(div4, t2);
    			append(div4, div2);
    			append(div2, t3);
    			append(div4, t4);
    			append(div4, div3);
    			append(div3, t5);
    			append(div4, t6);
    			if (if_block) if_block.m(div4, null);
    			append(div5, t7);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*agenda*/ 1 && !src_url_equal(img.src, img_src_value = /*photo*/ ctx[10])) {
    				attr(img, "src", img_src_value);
    			}

    			if (dirty & /*agenda*/ 1 && t1_value !== (t1_value = /*name*/ ctx[7] + "")) set_data(t1, t1_value);
    			if (dirty & /*agenda*/ 1 && t3_value !== (t3_value = /*designation*/ ctx[8] + "")) set_data(t3, t3_value);
    			if (dirty & /*agenda*/ 1 && t5_value !== (t5_value = /*organisation*/ ctx[9] + "")) set_data(t5, t5_value);

    			if (/*linkedin*/ ctx[11]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block_3$1(ctx);
    					if_block.c();
    					if_block.m(div4, null);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div5);
    			if (if_block) if_block.d();
    		}
    	};
    }

    // (43:4) {#each Object.entries(agenda.speakers) as [category, speaker_list]}
    function create_each_block$5(ctx) {
    	let div2;
    	let div0;
    	let t0_value = simplur__default["default"]`${[/*speaker_list*/ ctx[4].length, null]}${/*category*/ ctx[3]}[|s]` + "";
    	let t0;
    	let t1;
    	let div1;
    	let t2;
    	let each_value_1 = /*speaker_list*/ ctx[4];
    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1$2(get_each_context_1$2(ctx, each_value_1, i));
    	}

    	return {
    		c() {
    			div2 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div1 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t2 = space();
    			attr(div0, "class", "speakers__category");
    			attr(div1, "class", "speakers__list horizontal");
    			attr(div2, "class", "speakers__container");
    		},
    		m(target, anchor) {
    			insert(target, div2, anchor);
    			append(div2, div0);
    			append(div0, t0);
    			append(div2, t1);
    			append(div2, div1);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div1, null);
    			}

    			append(div2, t2);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*agenda*/ 1 && t0_value !== (t0_value = simplur__default["default"]`${[/*speaker_list*/ ctx[4].length, null]}${/*category*/ ctx[3]}[|s]` + "")) set_data(t0, t0_value);

    			if (dirty & /*Object, agenda*/ 1) {
    				each_value_1 = /*speaker_list*/ ctx[4];
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1$2(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_1$2(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div1, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_1.length;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div2);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (93:2) {#if agenda.venue}
    function create_if_block_1$2(ctx) {
    	let t0;
    	let span;
    	let t1_value = /*agenda*/ ctx[0].venue + "";
    	let t1;

    	return {
    		c() {
    			t0 = text("Venue: ");
    			span = element("span");
    			t1 = text(t1_value);
    		},
    		m(target, anchor) {
    			insert(target, t0, anchor);
    			insert(target, span, anchor);
    			append(span, t1);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*agenda*/ 1 && t1_value !== (t1_value = /*agenda*/ ctx[0].venue + "")) set_data(t1, t1_value);
    		},
    		d(detaching) {
    			if (detaching) detach(t0);
    			if (detaching) detach(span);
    		}
    	};
    }

    // (96:2) {#if agenda.category}
    function create_if_block$2(ctx) {
    	let div;
    	let t_value = /*agenda*/ ctx[0].category + "";
    	let t;
    	let div_class_value;

    	return {
    		c() {
    			div = element("div");
    			t = text(t_value);

    			attr(div, "class", div_class_value = "event__category " + (/*agenda*/ ctx[0].category
    			? /*categories*/ ctx[1].indexOf(/*agenda*/ ctx[0].category) >= 0
    				? 'cat-' + /*categories*/ ctx[1].indexOf(/*agenda*/ ctx[0].category)
    				: ''
    			: ''));
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*agenda*/ 1 && t_value !== (t_value = /*agenda*/ ctx[0].category + "")) set_data(t, t_value);

    			if (dirty & /*agenda, categories*/ 3 && div_class_value !== (div_class_value = "event__category " + (/*agenda*/ ctx[0].category
    			? /*categories*/ ctx[1].indexOf(/*agenda*/ ctx[0].category) >= 0
    				? 'cat-' + /*categories*/ ctx[1].indexOf(/*agenda*/ ctx[0].category)
    				: ''
    			: ''))) {
    				attr(div, "class", div_class_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    function create_fragment$6(ctx) {
    	let t0;
    	let show_if = Object.entries(/*agenda*/ ctx[0].speakers).length;
    	let t1;
    	let div1;
    	let div0;
    	let t2_value = /*date_detail*/ ctx[2](/*agenda*/ ctx[0].start_time, /*agenda*/ ctx[0].end_time) + "";
    	let t2;
    	let t3;
    	let t4;
    	let current;
    	let if_block0 = (/*agenda*/ ctx[0].description || /*agenda*/ ctx[0].link) && create_if_block_4$1(ctx);
    	let if_block1 = show_if && create_if_block_2$1(ctx);
    	let if_block2 = /*agenda*/ ctx[0].venue && create_if_block_1$2(ctx);
    	let if_block3 = /*agenda*/ ctx[0].category && create_if_block$2(ctx);

    	return {
    		c() {
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			div1 = element("div");
    			div0 = element("div");
    			t2 = text(t2_value);
    			t3 = space();
    			if (if_block2) if_block2.c();
    			t4 = space();
    			if (if_block3) if_block3.c();
    			attr(div1, "class", "event__details");
    		},
    		m(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert(target, t0, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert(target, t1, anchor);
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div0, t2);
    			append(div1, t3);
    			if (if_block2) if_block2.m(div1, null);
    			append(div1, t4);
    			if (if_block3) if_block3.m(div1, null);
    			current = true;
    		},
    		p(ctx, [dirty]) {
    			if (/*agenda*/ ctx[0].description || /*agenda*/ ctx[0].link) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);

    					if (dirty & /*agenda*/ 1) {
    						transition_in(if_block0, 1);
    					}
    				} else {
    					if_block0 = create_if_block_4$1(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(t0.parentNode, t0);
    				}
    			} else if (if_block0) {
    				group_outros();

    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});

    				check_outros();
    			}

    			if (dirty & /*agenda*/ 1) show_if = Object.entries(/*agenda*/ ctx[0].speakers).length;

    			if (show_if) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_2$1(ctx);
    					if_block1.c();
    					if_block1.m(t1.parentNode, t1);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if ((!current || dirty & /*agenda*/ 1) && t2_value !== (t2_value = /*date_detail*/ ctx[2](/*agenda*/ ctx[0].start_time, /*agenda*/ ctx[0].end_time) + "")) set_data(t2, t2_value);

    			if (/*agenda*/ ctx[0].venue) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block_1$2(ctx);
    					if_block2.c();
    					if_block2.m(div1, t4);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if (/*agenda*/ ctx[0].category) {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);
    				} else {
    					if_block3 = create_if_block$2(ctx);
    					if_block3.c();
    					if_block3.m(div1, null);
    				}
    			} else if (if_block3) {
    				if_block3.d(1);
    				if_block3 = null;
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(if_block0);
    			current = true;
    		},
    		o(local) {
    			transition_out(if_block0);
    			current = false;
    		},
    		d(detaching) {
    			if (if_block0) if_block0.d(detaching);
    			if (detaching) detach(t0);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach(t1);
    			if (detaching) detach(div1);
    			if (if_block2) if_block2.d();
    			if (if_block3) if_block3.d();
    		}
    	};
    }

    function instance$6($$self, $$props, $$invalidate) {
    	let { agenda } = $$props;
    	let { categories } = $$props;

    	function date_detail(start, end) {
    		let s = luxon.DateTime.fromSQL(start);
    		let e = luxon.DateTime.fromSQL(end);

    		if (s.hasSame(e, 'day')) {
    			return s.toFormat('cccc LLLL d, y hh:mm a') + " - " + e.toFormat('hh:mm a');
    		} else {
    			return s.toFormat('cccc LLLL d, y hh:mm a') + " - " + e.toFormat('cccc LLLL d, y hh:mm a');
    		}
    	}

    	$$self.$$set = $$props => {
    		if ('agenda' in $$props) $$invalidate(0, agenda = $$props.agenda);
    		if ('categories' in $$props) $$invalidate(1, categories = $$props.categories);
    	};

    	return [agenda, categories, date_detail];
    }

    class Details extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$6, create_fragment$6, safe_not_equal, { agenda: 0, categories: 1 });
    	}
    }

    /* src/Detailed/Detailed.svelte generated by Svelte v3.47.0 */

    function get_each_context$4(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[2] = list[i];
    	return child_ctx;
    }

    // (10:2) {#each agenda as item }
    function create_each_block$4(ctx) {
    	let div1;
    	let div0;
    	let t0_value = /*item*/ ctx[2].name + "";
    	let t0;
    	let div0_class_value;
    	let t1;
    	let details;
    	let t2;
    	let current;

    	details = new Details({
    			props: {
    				agenda: /*item*/ ctx[2],
    				categories: /*categories*/ ctx[1]
    			}
    		});

    	return {
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			create_component(details.$$.fragment);
    			t2 = space();

    			attr(div0, "class", div0_class_value = "event__name " + (/*item*/ ctx[2].category
    			? /*categories*/ ctx[1].indexOf(/*item*/ ctx[2].category) >= 0
    				? 'cat-' + /*categories*/ ctx[1].indexOf(/*item*/ ctx[2].category)
    				: ''
    			: ''));

    			attr(div1, "class", "event");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div0, t0);
    			append(div1, t1);
    			mount_component(details, div1, null);
    			append(div1, t2);
    			current = true;
    		},
    		p(ctx, dirty) {
    			if ((!current || dirty & /*agenda*/ 1) && t0_value !== (t0_value = /*item*/ ctx[2].name + "")) set_data(t0, t0_value);

    			if (!current || dirty & /*agenda, categories*/ 3 && div0_class_value !== (div0_class_value = "event__name " + (/*item*/ ctx[2].category
    			? /*categories*/ ctx[1].indexOf(/*item*/ ctx[2].category) >= 0
    				? 'cat-' + /*categories*/ ctx[1].indexOf(/*item*/ ctx[2].category)
    				: ''
    			: ''))) {
    				attr(div0, "class", div0_class_value);
    			}

    			const details_changes = {};
    			if (dirty & /*agenda*/ 1) details_changes.agenda = /*item*/ ctx[2];
    			if (dirty & /*categories*/ 2) details_changes.categories = /*categories*/ ctx[1];
    			details.$set(details_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(details.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(details.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div1);
    			destroy_component(details);
    		}
    	};
    }

    function create_fragment$5(ctx) {
    	let div;
    	let current;
    	let each_value = /*agenda*/ ctx[0];
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$4(get_each_context$4(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	return {
    		c() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "event__cover");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p(ctx, [dirty]) {
    			if (dirty & /*agenda, categories*/ 3) {
    				each_value = /*agenda*/ ctx[0];
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$4(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$4(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    function instance$5($$self, $$props, $$invalidate) {
    	let { agenda } = $$props;
    	let { categories } = $$props;

    	$$self.$$set = $$props => {
    		if ('agenda' in $$props) $$invalidate(0, agenda = $$props.agenda);
    		if ('categories' in $$props) $$invalidate(1, categories = $$props.categories);
    	};

    	return [agenda, categories];
    }

    class Detailed extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, { agenda: 0, categories: 1 });
    	}
    }

    /* src/Detailed/Event.svelte generated by Svelte v3.47.0 */

    function create_fragment$4(ctx) {
    	let div1;
    	let t;
    	let div0;
    	let details;
    	let div1_class_value;
    	let current;
    	const default_slot_template = /*#slots*/ ctx[3].default;
    	const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[2], null);

    	details = new Details({
    			props: {
    				agenda: /*agenda*/ ctx[0],
    				categories: /*categories*/ ctx[1]
    			}
    		});

    	return {
    		c() {
    			div1 = element("div");
    			if (default_slot) default_slot.c();
    			t = space();
    			div0 = element("div");
    			create_component(details.$$.fragment);
    			attr(div0, "class", "detailed__box");

    			attr(div1, "class", div1_class_value = "event__name " + (/*agenda*/ ctx[0].category
    			? /*categories*/ ctx[1].indexOf(/*agenda*/ ctx[0].category) >= 0
    				? 'cat-' + /*categories*/ ctx[1].indexOf(/*agenda*/ ctx[0].category)
    				: ''
    			: ''));
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);

    			if (default_slot) {
    				default_slot.m(div1, null);
    			}

    			append(div1, t);
    			append(div1, div0);
    			mount_component(details, div0, null);
    			current = true;
    		},
    		p(ctx, [dirty]) {
    			if (default_slot) {
    				if (default_slot.p && (!current || dirty & /*$$scope*/ 4)) {
    					update_slot_base(
    						default_slot,
    						default_slot_template,
    						ctx,
    						/*$$scope*/ ctx[2],
    						!current
    						? get_all_dirty_from_scope(/*$$scope*/ ctx[2])
    						: get_slot_changes(default_slot_template, /*$$scope*/ ctx[2], dirty, null),
    						null
    					);
    				}
    			}

    			const details_changes = {};
    			if (dirty & /*agenda*/ 1) details_changes.agenda = /*agenda*/ ctx[0];
    			if (dirty & /*categories*/ 2) details_changes.categories = /*categories*/ ctx[1];
    			details.$set(details_changes);

    			if (!current || dirty & /*agenda, categories*/ 3 && div1_class_value !== (div1_class_value = "event__name " + (/*agenda*/ ctx[0].category
    			? /*categories*/ ctx[1].indexOf(/*agenda*/ ctx[0].category) >= 0
    				? 'cat-' + /*categories*/ ctx[1].indexOf(/*agenda*/ ctx[0].category)
    				: ''
    			: ''))) {
    				attr(div1, "class", div1_class_value);
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(default_slot, local);
    			transition_in(details.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(default_slot, local);
    			transition_out(details.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div1);
    			if (default_slot) default_slot.d(detaching);
    			destroy_component(details);
    		}
    	};
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	let { agenda } = $$props;
    	let { categories } = $$props;

    	onMount(() => {
    		console.log(agenda);
    	});

    	$$self.$$set = $$props => {
    		if ('agenda' in $$props) $$invalidate(0, agenda = $$props.agenda);
    		if ('categories' in $$props) $$invalidate(1, categories = $$props.categories);
    		if ('$$scope' in $$props) $$invalidate(2, $$scope = $$props.$$scope);
    	};

    	return [agenda, categories, $$scope, slots];
    }

    class Event extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, { agenda: 0, categories: 1 });
    	}
    }

    /* src/Detailed/Simple.svelte generated by Svelte v3.47.0 */

    function get_each_context$3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[2] = list[i];
    	return child_ctx;
    }

    // (9:4) <Event agenda={item} categories={categories}>
    function create_default_slot$1(ctx) {
    	let t0_value = /*item*/ ctx[2].name + "";
    	let t0;
    	let t1;

    	return {
    		c() {
    			t0 = text(t0_value);
    			t1 = space();
    		},
    		m(target, anchor) {
    			insert(target, t0, anchor);
    			insert(target, t1, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*agenda*/ 1 && t0_value !== (t0_value = /*item*/ ctx[2].name + "")) set_data(t0, t0_value);
    		},
    		d(detaching) {
    			if (detaching) detach(t0);
    			if (detaching) detach(t1);
    		}
    	};
    }

    // (8:2) {#each agenda as item }
    function create_each_block$3(ctx) {
    	let event;
    	let current;

    	event = new Event({
    			props: {
    				agenda: /*item*/ ctx[2],
    				categories: /*categories*/ ctx[1],
    				$$slots: { default: [create_default_slot$1] },
    				$$scope: { ctx }
    			}
    		});

    	return {
    		c() {
    			create_component(event.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(event, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const event_changes = {};
    			if (dirty & /*agenda*/ 1) event_changes.agenda = /*item*/ ctx[2];
    			if (dirty & /*categories*/ 2) event_changes.categories = /*categories*/ ctx[1];

    			if (dirty & /*$$scope, agenda*/ 33) {
    				event_changes.$$scope = { dirty, ctx };
    			}

    			event.$set(event_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(event.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(event.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(event, detaching);
    		}
    	};
    }

    function create_fragment$3(ctx) {
    	let div;
    	let current;
    	let each_value = /*agenda*/ ctx[0];
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$3(get_each_context$3(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	return {
    		c() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "event simple");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p(ctx, [dirty]) {
    			if (dirty & /*agenda, categories*/ 3) {
    				each_value = /*agenda*/ ctx[0];
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$3(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$3(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let { agenda } = $$props;
    	let { categories } = $$props;

    	$$self.$$set = $$props => {
    		if ('agenda' in $$props) $$invalidate(0, agenda = $$props.agenda);
    		if ('categories' in $$props) $$invalidate(1, categories = $$props.categories);
    	};

    	return [agenda, categories];
    }

    class Simple extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, { agenda: 0, categories: 1 });
    	}
    }

    /* src/Detailed/Venue.svelte generated by Svelte v3.47.0 */

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[3] = list[i];
    	return child_ctx;
    }

    // (17:6) <Event agenda={item} categories={categories}>
    function create_default_slot(ctx) {
    	let span;
    	let t0_value = /*date_sql*/ ctx[2](/*item*/ ctx[3].start_time, 'hh:mm a') + "";
    	let t0;
    	let t1;
    	let t2_value = /*item*/ ctx[3].name + "";
    	let t2;

    	return {
    		c() {
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = text(" - ");
    			t2 = text(t2_value);
    		},
    		m(target, anchor) {
    			insert(target, span, anchor);
    			append(span, t0);
    			insert(target, t1, anchor);
    			insert(target, t2, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*agenda*/ 1 && t0_value !== (t0_value = /*date_sql*/ ctx[2](/*item*/ ctx[3].start_time, 'hh:mm a') + "")) set_data(t0, t0_value);
    			if (dirty & /*agenda*/ 1 && t2_value !== (t2_value = /*item*/ ctx[3].name + "")) set_data(t2, t2_value);
    		},
    		d(detaching) {
    			if (detaching) detach(span);
    			if (detaching) detach(t1);
    			if (detaching) detach(t2);
    		}
    	};
    }

    // (15:2) {#each agenda as item }
    function create_each_block$2(ctx) {
    	let div;
    	let event;
    	let t;
    	let current;

    	event = new Event({
    			props: {
    				agenda: /*item*/ ctx[3],
    				categories: /*categories*/ ctx[1],
    				$$slots: { default: [create_default_slot] },
    				$$scope: { ctx }
    			}
    		});

    	return {
    		c() {
    			div = element("div");
    			create_component(event.$$.fragment);
    			t = space();
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			mount_component(event, div, null);
    			append(div, t);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const event_changes = {};
    			if (dirty & /*agenda*/ 1) event_changes.agenda = /*item*/ ctx[3];
    			if (dirty & /*categories*/ 2) event_changes.categories = /*categories*/ ctx[1];

    			if (dirty & /*$$scope, agenda*/ 65) {
    				event_changes.$$scope = { dirty, ctx };
    			}

    			event.$set(event_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(event.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(event.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_component(event);
    		}
    	};
    }

    function create_fragment$2(ctx) {
    	let div;
    	let current;
    	let each_value = /*agenda*/ ctx[0];
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$2(get_each_context$2(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	return {
    		c() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "event venue");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p(ctx, [dirty]) {
    			if (dirty & /*agenda, categories, date_sql*/ 7) {
    				each_value = /*agenda*/ ctx[0];
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$2(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$2(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let { agenda } = $$props;
    	let { categories } = $$props;

    	function date_sql(date, format) {
    		let d = luxon.DateTime.fromSQL(date);
    		return d.toFormat(format);
    	}

    	$$self.$$set = $$props => {
    		if ('agenda' in $$props) $$invalidate(0, agenda = $$props.agenda);
    		if ('categories' in $$props) $$invalidate(1, categories = $$props.categories);
    	};

    	return [agenda, categories, date_sql];
    }

    class Venue extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, { agenda: 0, categories: 1 });
    	}
    }

    /* src/Detailed/Filters.svelte generated by Svelte v3.47.0 */

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[5] = list[i];
    	return child_ctx;
    }

    function get_each_context_1$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[8] = list[i];
    	return child_ctx;
    }

    // (11:0) {#if categories.length}
    function create_if_block_1$1(ctx) {
    	let div0;
    	let t1;
    	let div1;
    	let each_value_1 = /*categories*/ ctx[0];
    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1$1(get_each_context_1$1(ctx, each_value_1, i));
    	}

    	return {
    		c() {
    			div0 = element("div");
    			div0.textContent = "Filters Category";
    			t1 = space();
    			div1 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div0, "class", "filter__heading");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			insert(target, t1, anchor);
    			insert(target, div1, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div1, null);
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*categories, dispatch*/ 5) {
    				each_value_1 = /*categories*/ ctx[0];
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1$1(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_1$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div1, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_1.length;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div0);
    			if (detaching) detach(t1);
    			if (detaching) detach(div1);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (14:4) {#each categories as category}
    function create_each_block_1$1(ctx) {
    	let button;
    	let t0_value = /*category*/ ctx[8] + "";
    	let t0;
    	let t1;
    	let button_class_value;
    	let mounted;
    	let dispose;

    	function click_handler() {
    		return /*click_handler*/ ctx[3](/*category*/ ctx[8]);
    	}

    	return {
    		c() {
    			button = element("button");
    			t0 = text(t0_value);
    			t1 = space();

    			attr(button, "class", button_class_value = "event__category " + (/*category*/ ctx[8]
    			? /*categories*/ ctx[0].indexOf(/*category*/ ctx[8]) >= 0
    				? 'cat-' + /*categories*/ ctx[0].indexOf(/*category*/ ctx[8])
    				: ''
    			: ''));
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t0);
    			append(button, t1);

    			if (!mounted) {
    				dispose = listen(button, "click", click_handler);
    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*categories*/ 1 && t0_value !== (t0_value = /*category*/ ctx[8] + "")) set_data(t0, t0_value);

    			if (dirty & /*categories*/ 1 && button_class_value !== (button_class_value = "event__category " + (/*category*/ ctx[8]
    			? /*categories*/ ctx[0].indexOf(/*category*/ ctx[8]) >= 0
    				? 'cat-' + /*categories*/ ctx[0].indexOf(/*category*/ ctx[8])
    				: ''
    			: ''))) {
    				attr(button, "class", button_class_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(button);
    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (22:0) {#if venues.length}
    function create_if_block$1(ctx) {
    	let div0;
    	let t1;
    	let div1;
    	let each_value = /*venues*/ ctx[1];
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	return {
    		c() {
    			div0 = element("div");
    			div0.textContent = "Filters Venue";
    			t1 = space();
    			div1 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div0, "class", "filter__heading");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			insert(target, t1, anchor);
    			insert(target, div1, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div1, null);
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*dispatch, venues*/ 6) {
    				each_value = /*venues*/ ctx[1];
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div1, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div0);
    			if (detaching) detach(t1);
    			if (detaching) detach(div1);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (25:4) {#each venues as venue}
    function create_each_block$1(ctx) {
    	let button;
    	let t0_value = /*venue*/ ctx[5] + "";
    	let t0;
    	let t1;
    	let mounted;
    	let dispose;

    	function click_handler_1() {
    		return /*click_handler_1*/ ctx[4](/*venue*/ ctx[5]);
    	}

    	return {
    		c() {
    			button = element("button");
    			t0 = text(t0_value);
    			t1 = space();
    			attr(button, "class", "venue_filter");
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, t0);
    			append(button, t1);

    			if (!mounted) {
    				dispose = listen(button, "click", click_handler_1);
    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*venues*/ 2 && t0_value !== (t0_value = /*venue*/ ctx[5] + "")) set_data(t0, t0_value);
    		},
    		d(detaching) {
    			if (detaching) detach(button);
    			mounted = false;
    			dispose();
    		}
    	};
    }

    function create_fragment$1(ctx) {
    	let t;
    	let if_block1_anchor;
    	let if_block0 = /*categories*/ ctx[0].length && create_if_block_1$1(ctx);
    	let if_block1 = /*venues*/ ctx[1].length && create_if_block$1(ctx);

    	return {
    		c() {
    			if (if_block0) if_block0.c();
    			t = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    		},
    		m(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert(target, t, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert(target, if_block1_anchor, anchor);
    		},
    		p(ctx, [dirty]) {
    			if (/*categories*/ ctx[0].length) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_1$1(ctx);
    					if_block0.c();
    					if_block0.m(t.parentNode, t);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*venues*/ ctx[1].length) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block$1(ctx);
    					if_block1.c();
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (if_block0) if_block0.d(detaching);
    			if (detaching) detach(t);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach(if_block1_anchor);
    		}
    	};
    }

    function instance$1($$self, $$props, $$invalidate) {
    	const dispatch = createEventDispatcher();
    	let { categories } = $$props;
    	let { venues } = $$props;

    	const click_handler = category => {
    		dispatch('category', category);
    	};

    	const click_handler_1 = venue => {
    		dispatch('venue', venue);
    	};

    	$$self.$$set = $$props => {
    		if ('categories' in $$props) $$invalidate(0, categories = $$props.categories);
    		if ('venues' in $$props) $$invalidate(1, venues = $$props.venues);
    	};

    	return [categories, venues, dispatch, click_handler, click_handler_1];
    }

    class Filters extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, { categories: 0, venues: 1 });
    	}
    }

    /* src/Detailed/Agenda.svelte generated by Svelte v3.47.0 */

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[23] = list[i][0];
    	child_ctx[24] = list[i][1];
    	return child_ctx;
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[27] = list[i][0];
    	child_ctx[28] = list[i][1];
    	return child_ctx;
    }

    function get_each_context_2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[23] = list[i][0];
    	child_ctx[24] = list[i][1];
    	return child_ctx;
    }

    // (79:4) {#each Object.entries(agenda_list) as [date, agendas]}
    function create_each_block_2(ctx) {
    	let button;
    	let span;
    	let t0_value = /*date_format*/ ctx[9](/*date*/ ctx[23], 'ccc') + "";
    	let t0;
    	let t1;
    	let t2_value = /*date_format*/ ctx[9](/*date*/ ctx[23], 'LLL d, y') + "";
    	let t2;
    	let t3;
    	let button_class_value;
    	let mounted;
    	let dispose;

    	function click_handler() {
    		return /*click_handler*/ ctx[16](/*date*/ ctx[23]);
    	}

    	return {
    		c() {
    			button = element("button");
    			span = element("span");
    			t0 = text(t0_value);
    			t1 = text(", ");
    			t2 = text(t2_value);
    			t3 = space();

    			attr(button, "class", button_class_value = "" + ((/*date*/ ctx[23] == /*active_tab*/ ctx[4]
    			? /*color*/ ctx[0]
    			: '') + ""));
    		},
    		m(target, anchor) {
    			insert(target, button, anchor);
    			append(button, span);
    			append(span, t0);
    			append(button, t1);
    			append(button, t2);
    			append(button, t3);

    			if (!mounted) {
    				dispose = listen(button, "click", click_handler);
    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty[0] & /*agenda_list*/ 4 && t0_value !== (t0_value = /*date_format*/ ctx[9](/*date*/ ctx[23], 'ccc') + "")) set_data(t0, t0_value);
    			if (dirty[0] & /*agenda_list*/ 4 && t2_value !== (t2_value = /*date_format*/ ctx[9](/*date*/ ctx[23], 'LLL d, y') + "")) set_data(t2, t2_value);

    			if (dirty[0] & /*agenda_list, active_tab, color*/ 21 && button_class_value !== (button_class_value = "" + ((/*date*/ ctx[23] == /*active_tab*/ ctx[4]
    			? /*color*/ ctx[0]
    			: '') + ""))) {
    				attr(button, "class", button_class_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(button);
    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (88:2) {#if filter_category}
    function create_if_block_9(ctx) {
    	let div;
    	let t0;
    	let t1;
    	let t2;
    	let button;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			div = element("div");
    			t0 = text("Category: ");
    			t1 = text(/*filter_category*/ ctx[5]);
    			t2 = space();
    			button = element("button");
    			button.textContent = "✗";
    			attr(button, "class", "close");
    			attr(div, "class", "filter__item");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t0);
    			append(div, t1);
    			append(div, t2);
    			append(div, button);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler_1*/ ctx[17]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty[0] & /*filter_category*/ 32) set_data(t1, /*filter_category*/ ctx[5]);
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (94:2) {#if filter_venue}
    function create_if_block_8(ctx) {
    	let div;
    	let t0;
    	let t1;
    	let t2;
    	let button;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			div = element("div");
    			t0 = text("Venue: ");
    			t1 = text(/*filter_venue*/ ctx[6]);
    			t2 = space();
    			button = element("button");
    			button.textContent = "✗";
    			attr(button, "class", "close");
    			attr(div, "class", "filter__item");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t0);
    			append(div, t1);
    			append(div, t2);
    			append(div, button);

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler_2*/ ctx[18]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty[0] & /*filter_venue*/ 64) set_data(t1, /*filter_venue*/ ctx[6]);
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (110:0) {:else}
    function create_else_block(ctx) {
    	let div1;
    	let show_if = Object.entries(/*agenda_list*/ ctx[2]).length < 1;
    	let t0;
    	let t1;
    	let div0;
    	let filters;
    	let current;
    	let if_block = show_if && create_if_block_7();
    	let each_value = Object.entries(/*agenda_list*/ ctx[2]);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	filters = new Filters({
    			props: {
    				categories: /*categories*/ ctx[7],
    				venues: /*venues*/ ctx[8]
    			}
    		});

    	filters.$on("category", /*category_handler*/ ctx[19]);
    	filters.$on("venue", /*venue_handler*/ ctx[20]);

    	return {
    		c() {
    			div1 = element("div");
    			if (if_block) if_block.c();
    			t0 = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t1 = space();
    			div0 = element("div");
    			create_component(filters.$$.fragment);
    			attr(div0, "class", "agenda__filters");
    			attr(div1, "class", "agenda__box");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			if (if_block) if_block.m(div1, null);
    			append(div1, t0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div1, null);
    			}

    			append(div1, t1);
    			append(div1, div0);
    			mount_component(filters, div0, null);
    			current = true;
    		},
    		p(ctx, dirty) {
    			if (dirty[0] & /*agenda_list*/ 4) show_if = Object.entries(/*agenda_list*/ ctx[2]).length < 1;

    			if (show_if) {
    				if (if_block) ; else {
    					if_block = create_if_block_7();
    					if_block.c();
    					if_block.m(div1, t0);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}

    			if (dirty[0] & /*agenda_list, categories, type, date_sql, active_tab*/ 1174) {
    				each_value = Object.entries(/*agenda_list*/ ctx[2]);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div1, t1);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}

    			const filters_changes = {};
    			if (dirty[0] & /*categories*/ 128) filters_changes.categories = /*categories*/ ctx[7];
    			if (dirty[0] & /*venues*/ 256) filters_changes.venues = /*venues*/ ctx[8];
    			filters.$set(filters_changes);
    		},
    		i(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			transition_in(filters.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			transition_out(filters.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div1);
    			if (if_block) if_block.d();
    			destroy_each(each_blocks, detaching);
    			destroy_component(filters);
    		}
    	};
    }

    // (103:0) {#if loading}
    function create_if_block(ctx) {
    	let div;

    	return {
    		c() {
    			div = element("div");
    			div.innerHTML = `<svg class="" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle><path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path></svg>`;
    			attr(div, "class", "loading");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (113:4) {#if Object.entries(agenda_list).length < 1 }
    function create_if_block_7(ctx) {
    	let div;

    	return {
    		c() {
    			div = element("div");
    			div.textContent = "No events to show. Try clearing filters.";
    			attr(div, "class", "agenda__container");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (121:6) {#if date == active_tab}
    function create_if_block_1(ctx) {
    	let div;
    	let show_if = Object.entries(/*agendas*/ ctx[24]).length < 1;
    	let t;
    	let current;
    	let if_block = show_if && create_if_block_6();
    	let each_value_1 = Object.entries(/*agendas*/ ctx[24]);
    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	return {
    		c() {
    			div = element("div");
    			if (if_block) if_block.c();
    			t = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "agenda__container");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			if (if_block) if_block.m(div, null);
    			append(div, t);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p(ctx, dirty) {
    			if (dirty[0] & /*agenda_list*/ 4) show_if = Object.entries(/*agendas*/ ctx[24]).length < 1;

    			if (show_if) {
    				if (if_block) ; else {
    					if_block = create_if_block_6();
    					if_block.c();
    					if_block.m(div, t);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}

    			if (dirty[0] & /*agenda_list, categories, type, date_sql*/ 1158) {
    				each_value_1 = Object.entries(/*agendas*/ ctx[24]);
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block_1(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value_1.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i(local) {
    			if (current) return;

    			for (let i = 0; i < each_value_1.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			if (if_block) if_block.d();
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (125:10) {#if Object.entries(agendas).length < 1 }
    function create_if_block_6(ctx) {
    	let t;

    	return {
    		c() {
    			t = text("No events on selected day.");
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (134:14) {:else}
    function create_else_block_1(ctx) {
    	let div;
    	let t_value = /*date_sql*/ ctx[10](/*time*/ ctx[27], 'hh:mm a') + "";
    	let t;

    	return {
    		c() {
    			div = element("div");
    			t = text(t_value);
    			attr(div, "class", "event__date");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t);
    		},
    		p(ctx, dirty) {
    			if (dirty[0] & /*agenda_list*/ 4 && t_value !== (t_value = /*date_sql*/ ctx[10](/*time*/ ctx[27], 'hh:mm a') + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (132:14) {#if type == "venue"}
    function create_if_block_5(ctx) {
    	let div;
    	let t_value = /*time*/ ctx[27] + "";
    	let t;

    	return {
    		c() {
    			div = element("div");
    			t = text(t_value);
    			attr(div, "class", "event__date venue");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t);
    		},
    		p(ctx, dirty) {
    			if (dirty[0] & /*agenda_list*/ 4 && t_value !== (t_value = /*time*/ ctx[27] + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (140:14) {#if type=="expanded"}
    function create_if_block_4(ctx) {
    	let detailed;
    	let current;

    	detailed = new Detailed({
    			props: {
    				agenda: /*agenda*/ ctx[28],
    				categories: /*categories*/ ctx[7]
    			}
    		});

    	return {
    		c() {
    			create_component(detailed.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(detailed, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const detailed_changes = {};
    			if (dirty[0] & /*agenda_list*/ 4) detailed_changes.agenda = /*agenda*/ ctx[28];
    			if (dirty[0] & /*categories*/ 128) detailed_changes.categories = /*categories*/ ctx[7];
    			detailed.$set(detailed_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(detailed.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(detailed.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(detailed, detaching);
    		}
    	};
    }

    // (144:14) {#if type=="simple"}
    function create_if_block_3(ctx) {
    	let simple;
    	let current;

    	simple = new Simple({
    			props: {
    				agenda: /*agenda*/ ctx[28],
    				categories: /*categories*/ ctx[7]
    			}
    		});

    	return {
    		c() {
    			create_component(simple.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(simple, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const simple_changes = {};
    			if (dirty[0] & /*agenda_list*/ 4) simple_changes.agenda = /*agenda*/ ctx[28];
    			if (dirty[0] & /*categories*/ 128) simple_changes.categories = /*categories*/ ctx[7];
    			simple.$set(simple_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(simple.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(simple.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(simple, detaching);
    		}
    	};
    }

    // (148:14) {#if type=="venue"}
    function create_if_block_2(ctx) {
    	let venue;
    	let current;

    	venue = new Venue({
    			props: {
    				agenda: /*agenda*/ ctx[28],
    				categories: /*categories*/ ctx[7]
    			}
    		});

    	return {
    		c() {
    			create_component(venue.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(venue, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const venue_changes = {};
    			if (dirty[0] & /*agenda_list*/ 4) venue_changes.agenda = /*agenda*/ ctx[28];
    			if (dirty[0] & /*categories*/ 128) venue_changes.categories = /*categories*/ ctx[7];
    			venue.$set(venue_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(venue.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(venue.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(venue, detaching);
    		}
    	};
    }

    // (129:10) {#each Object.entries(agendas) as [time, agenda] }
    function create_each_block_1(ctx) {
    	let div;
    	let t0;
    	let t1;
    	let t2;
    	let current;

    	function select_block_type_1(ctx, dirty) {
    		if (/*type*/ ctx[1] == "venue") return create_if_block_5;
    		return create_else_block_1;
    	}

    	let current_block_type = select_block_type_1(ctx);
    	let if_block0 = current_block_type(ctx);
    	let if_block1 = /*type*/ ctx[1] == "expanded" && create_if_block_4(ctx);
    	let if_block2 = /*type*/ ctx[1] == "simple" && create_if_block_3(ctx);
    	let if_block3 = /*type*/ ctx[1] == "venue" && create_if_block_2(ctx);

    	return {
    		c() {
    			div = element("div");
    			if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			if (if_block2) if_block2.c();
    			t2 = space();
    			if (if_block3) if_block3.c();
    			attr(div, "class", "event__container");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			if_block0.m(div, null);
    			append(div, t0);
    			if (if_block1) if_block1.m(div, null);
    			append(div, t1);
    			if (if_block2) if_block2.m(div, null);
    			append(div, t2);
    			if (if_block3) if_block3.m(div, null);
    			current = true;
    		},
    		p(ctx, dirty) {
    			if (current_block_type === (current_block_type = select_block_type_1(ctx)) && if_block0) {
    				if_block0.p(ctx, dirty);
    			} else {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(div, t0);
    				}
    			}

    			if (/*type*/ ctx[1] == "expanded") {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);

    					if (dirty[0] & /*type*/ 2) {
    						transition_in(if_block1, 1);
    					}
    				} else {
    					if_block1 = create_if_block_4(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(div, t1);
    				}
    			} else if (if_block1) {
    				group_outros();

    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});

    				check_outros();
    			}

    			if (/*type*/ ctx[1] == "simple") {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);

    					if (dirty[0] & /*type*/ 2) {
    						transition_in(if_block2, 1);
    					}
    				} else {
    					if_block2 = create_if_block_3(ctx);
    					if_block2.c();
    					transition_in(if_block2, 1);
    					if_block2.m(div, t2);
    				}
    			} else if (if_block2) {
    				group_outros();

    				transition_out(if_block2, 1, 1, () => {
    					if_block2 = null;
    				});

    				check_outros();
    			}

    			if (/*type*/ ctx[1] == "venue") {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);

    					if (dirty[0] & /*type*/ 2) {
    						transition_in(if_block3, 1);
    					}
    				} else {
    					if_block3 = create_if_block_2(ctx);
    					if_block3.c();
    					transition_in(if_block3, 1);
    					if_block3.m(div, null);
    				}
    			} else if (if_block3) {
    				group_outros();

    				transition_out(if_block3, 1, 1, () => {
    					if_block3 = null;
    				});

    				check_outros();
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(if_block1);
    			transition_in(if_block2);
    			transition_in(if_block3);
    			current = true;
    		},
    		o(local) {
    			transition_out(if_block1);
    			transition_out(if_block2);
    			transition_out(if_block3);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			if_block0.d();
    			if (if_block1) if_block1.d();
    			if (if_block2) if_block2.d();
    			if (if_block3) if_block3.d();
    		}
    	};
    }

    // (119:4) {#each Object.entries(agenda_list) as [date, agendas]}
    function create_each_block(ctx) {
    	let if_block_anchor;
    	let current;
    	let if_block = /*date*/ ctx[23] == /*active_tab*/ ctx[4] && create_if_block_1(ctx);

    	return {
    		c() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		m(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			if (/*date*/ ctx[23] == /*active_tab*/ ctx[4]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);

    					if (dirty[0] & /*agenda_list, active_tab*/ 20) {
    						transition_in(if_block, 1);
    					}
    				} else {
    					if_block = create_if_block_1(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach(if_block_anchor);
    		}
    	};
    }

    function create_fragment(ctx) {
    	let div1;
    	let div0;
    	let t0;
    	let div2;
    	let t1;
    	let t2;
    	let current_block_type_index;
    	let if_block2;
    	let if_block2_anchor;
    	let current;
    	let each_value_2 = Object.entries(/*agenda_list*/ ctx[2]);
    	let each_blocks = [];

    	for (let i = 0; i < each_value_2.length; i += 1) {
    		each_blocks[i] = create_each_block_2(get_each_context_2(ctx, each_value_2, i));
    	}

    	let if_block0 = /*filter_category*/ ctx[5] && create_if_block_9(ctx);
    	let if_block1 = /*filter_venue*/ ctx[6] && create_if_block_8(ctx);
    	const if_block_creators = [create_if_block, create_else_block];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*loading*/ ctx[3]) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block2 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	return {
    		c() {
    			div1 = element("div");
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t0 = space();
    			div2 = element("div");
    			if (if_block0) if_block0.c();
    			t1 = space();
    			if (if_block1) if_block1.c();
    			t2 = space();
    			if_block2.c();
    			if_block2_anchor = empty();
    			attr(div0, "class", "agenda__tabs");
    			attr(div1, "class", "agenda__header");
    			attr(div2, "class", "filter__container");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div0, null);
    			}

    			insert(target, t0, anchor);
    			insert(target, div2, anchor);
    			if (if_block0) if_block0.m(div2, null);
    			append(div2, t1);
    			if (if_block1) if_block1.m(div2, null);
    			insert(target, t2, anchor);
    			if_blocks[current_block_type_index].m(target, anchor);
    			insert(target, if_block2_anchor, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			if (dirty[0] & /*agenda_list, active_tab, color, selectTab, date_format*/ 2581) {
    				each_value_2 = Object.entries(/*agenda_list*/ ctx[2]);
    				let i;

    				for (i = 0; i < each_value_2.length; i += 1) {
    					const child_ctx = get_each_context_2(ctx, each_value_2, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_2(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div0, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_2.length;
    			}

    			if (/*filter_category*/ ctx[5]) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_9(ctx);
    					if_block0.c();
    					if_block0.m(div2, t1);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*filter_venue*/ ctx[6]) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_8(ctx);
    					if_block1.c();
    					if_block1.m(div2, null);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(ctx, dirty);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block2 = if_blocks[current_block_type_index];

    				if (!if_block2) {
    					if_block2 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block2.c();
    				} else {
    					if_block2.p(ctx, dirty);
    				}

    				transition_in(if_block2, 1);
    				if_block2.m(if_block2_anchor.parentNode, if_block2_anchor);
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(if_block2);
    			current = true;
    		},
    		o(local) {
    			transition_out(if_block2);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div1);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach(t0);
    			if (detaching) detach(div2);
    			if (if_block0) if_block0.d();
    			if (if_block1) if_block1.d();
    			if (detaching) detach(t2);
    			if_blocks[current_block_type_index].d(detaching);
    			if (detaching) detach(if_block2_anchor);
    		}
    	};
    }

    function instance($$self, $$props, $$invalidate) {
    	let { event } = $$props;
    	let { url } = $$props;
    	let { color } = $$props;
    	let { type = 'simple' } = $$props;
    	let loading = false;
    	let active_tab = "";
    	let agenda_list = [];
    	let filter_category = "";
    	let filter_venue = "";
    	let categories = [];
    	let venues = [];

    	function dates() {
    		let dates = Object.keys(agenda_list);
    		return dates;
    	}

    	onMount(async () => {
    		var today = luxon.DateTime.now().toFormat("LLL dd, yyyy");
    		await loadData();
    		$$invalidate(4, active_tab = dates().indexOf(today) >= 0 ? today : dates()[0]);
    	});

    	async function loadData() {
    		$$invalidate(3, loading = true);
    		let data = await fetch(`${url}/api/event/${event}/agenda/${type}?venue=${filter_venue}&category=${filter_category}`).then(response => response.json());
    		$$invalidate(2, agenda_list = data.agenda);
    		$$invalidate(7, categories = data.categories);
    		$$invalidate(8, venues = data.venues);
    		$$invalidate(3, loading = false);
    	}

    	function date_format(date, format) {
    		let d = luxon.DateTime.fromFormat(date, "LLL dd, yyyy");
    		return d.toFormat(format);
    	}

    	function date_sql(date, format) {
    		let d = luxon.DateTime.fromSQL(date);
    		return d.toFormat(format);
    	}

    	function selectTab(date) {
    		$$invalidate(4, active_tab = date);
    	}

    	function changeCategory(item) {
    		$$invalidate(5, filter_category = item);
    		loadData();
    	}

    	function changeVenue(item) {
    		$$invalidate(6, filter_venue = item);
    		loadData();
    	}

    	const click_handler = date => selectTab(date);

    	const click_handler_1 = () => {
    		changeCategory("");
    	};

    	const click_handler_2 = () => {
    		changeVenue("");
    	};

    	const category_handler = event => {
    		changeCategory(event.detail);
    	};

    	const venue_handler = event => {
    		changeVenue(event.detail);
    	};

    	$$self.$$set = $$props => {
    		if ('event' in $$props) $$invalidate(14, event = $$props.event);
    		if ('url' in $$props) $$invalidate(15, url = $$props.url);
    		if ('color' in $$props) $$invalidate(0, color = $$props.color);
    		if ('type' in $$props) $$invalidate(1, type = $$props.type);
    	};

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty[0] & /*agenda_list*/ 4) ;
    	};

    	return [
    		color,
    		type,
    		agenda_list,
    		loading,
    		active_tab,
    		filter_category,
    		filter_venue,
    		categories,
    		venues,
    		date_format,
    		date_sql,
    		selectTab,
    		changeCategory,
    		changeVenue,
    		event,
    		url,
    		click_handler,
    		click_handler_1,
    		click_handler_2,
    		category_handler,
    		venue_handler
    	];
    }

    class Agenda extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance, create_fragment, safe_not_equal, { event: 14, url: 15, color: 0, type: 1 }, null, [-1, -1]);
    	}
    }

    return Agenda;

}));
