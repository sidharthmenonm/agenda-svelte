import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';

const pkg = require('./package.json');
const external = Object.keys(pkg.dependencies || {});

export default {
    input: 'src/Detailed/Agenda.svelte',
    output: [
        { 
            file: pkg.module, 
            'format': 'es',
            globals: {
                'luxon': 'luxon',
                'simplur': 'simplur',
                'svelte-markdown' : 'SvelteMarkdown'
            }
        },
        { 
            file: pkg.main, 
            'format': 'umd', 
            name: 'Agenda',
            globals: {
                'luxon': 'luxon',
                'simplur': 'simplur',
                'svelte-markdown' : 'SvelteMarkdown'
            }
        }
    ],
    external:[
        'luxon',
        'svelte-markdown',
        'simplur'
    ],
    plugins: [
        svelte(),
        resolve()
    ],
  };